﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace ChatoCrypto
{
	public partial class SideC : MasterDetailPage
	{
		ObservableCollection<string> Items;
		ObservableCollection<string> Users;

		public SideC ()
		{
			Items = new ObservableCollection<string> ();
			Users = new ObservableCollection<string> ();
			InitializeComponent ();
			bt1.IsEnabled = false;
			bt2.IsEnabled = false;
			bt3.IsEnabled = false;


			MessagingCenter.Subscribe<Login, string> ( this, "mensa", async (Login, args) => {
				Xamarin.Forms.Device.BeginInvokeOnMainThread(delegate {
					Items.Add(args);
					vaipofundo();
				});

			});
			MessagingCenter.Subscribe<Login, string> ( this, "du", async (Login, args) => {
				Xamarin.Forms.Device.BeginInvokeOnMainThread(delegate {
					Users.Add(args);

				});

			});
			//

			this.IsPresented=true;

			MessagingCenter.Subscribe<SideC, string> (this, "mostrar", async (Login, args) => {
				this.IsPresented=true;
			});
			MessagingCenter.Subscribe<SideC, string> (this, "esconde", async (Login, args) => {
				this.IsPresented=false;
			});


			listView.ItemsSource = Items;
			listView.ItemAppearing+= (object sender, ItemVisibilityEventArgs e) => {
				

			};
			Users = Cliente.getInstance (this).listaU ();
			listView2.ItemsSource = Users;
			//BindingContext = new {
			//	Menu = new { Subtitle = "I'm Master" },
			//	Detailpage = new { Subtitle = "I'm Detail" }
			//};
		}

		void OnEnviaClicked(object sender, EventArgs e){
			if (Cliente.getInstance (this).estaNaLista (entUser.Text)) {
				Cliente.enviaToUsr (entUser.Text, entMsg.Text.Substring(0,128));
				Items.Add ("Eu: " + entMsg.Text.Substring(0,128));
				vaipofundo ();
			} else {
				Items.Add ("Não tens a chave de " + entUser.Text + "");
				vaipofundo ();
			}
		}

		void vaipofundo(){
			if (Items.Count > 10) {try{
				var ultimo = Items [Items.Count-1];
					listView.ScrollTo (ultimo, ScrollToPosition.MakeVisible, false);
				}catch{
				}
				}
		}

		void OnEnviaChClicked(object sender, EventArgs e){
			Cliente.EnviaMyChaveUser (null, entUser.Text);
			Items.Add ("Chave Publica Enviada a "+entUser.Text );
			Items.Add ("Verifica se a Chave enviada corresponde á recebida:");
			Items.Add (Cliente.getInstance (this).myhash);
			vaipofundo ();
		}
		void OnPedeClicked(object sender, EventArgs e){
			if (Cliente.getInstance (this).estaNaLista (entUser.Text)) {
				Items.Add ("Já estava nos teus contactos");
				vaipofundo ();
			} else {
				Cliente.enviaToUsr (entUser.Text,"§P£");

			}
		}



		public void OnItemSelected (object sender, SelectedItemChangedEventArgs e) {
			if (e.SelectedItem == null) return; // has been set to null, do not 'process' tapped event
			DisplayAlert("", e.SelectedItem.ToString(), "OK");
			((ListView)sender).SelectedItem = null; // de-select the row
		}
		public void OnItemSelected2 (object sender, SelectedItemChangedEventArgs e) {
			if (e.SelectedItem == null) return; // has been set to null, do not 'process' tapped event
			entUser.Text = e.SelectedItem.ToString ();
			((ListView)sender).SelectedItem = null; // de-select the row
			this.IsPresented=false;
			bt1.IsEnabled = true;
			bt2.IsEnabled = true;
			bt3.IsEnabled = true;
		}

	}
}

