using System;
using Xamarin.Forms;

namespace ChatoCrypto
{
	public interface ICliente
	{
		string login (string user, string pwd);
		void settings (string ip, string porta);
		void reg (string user, string pwd);

	}

}

