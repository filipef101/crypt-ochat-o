﻿using System;
using System.Collections.Generic;
using XLabs.Platform;
using XLabs;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using Connectivity.Plugin.Abstractions;
using Connectivity.Plugin;
using System.Threading.Tasks;

namespace ChatoCrypto
{
	public partial class Login : ContentPage
	{
		public Login ()
		{
			InitializeComponent ();	
			usernameEntry.Text = Refractored.Xam.Settings.CrossSettings.Current.GetValueOrDefault("username","" );

		}
		public void paraPensar(){
			Xamarin.Forms.Device.BeginInvokeOnMainThread(delegate {
			loadE.IsRunning = false;
			loadE.IsVisible = false;
				btL.IsEnabled = true;
				btS.IsEnabled = true;
			});
		}

		
		void OnLoginClicked (object sender, EventArgs e)
		{Refractored.Xam.Settings.CrossSettings.Current.AddOrUpdateValue ("username", usernameEntry.Text);
			if (Connectivity.Plugin.CrossConnectivity.Current.IsConnected) {


				loadE.IsVisible = true;
				loadE.IsRunning = true;
				btL.IsEnabled = false;
				btS.IsEnabled = false;

				MessagingCenter.Subscribe<Login, string> (this, "estado", async (Login, args) => {
					Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
						estadoLbl.Text = args;
					});

				});

				//MessagingCenter.Subscribe<Login, string> (this, "Hi", (sender) => {
				// do something whenever the "Hi" message is sent
				// using the 'arg' parameter which is a string
				MessagingCenter.Subscribe<Login, string> (this, "log", async (Login, args) => {
					// do something whenever the "Hi" message is sent

					switch (args) {


					case("Logado"):
						Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
							DisplayAlert ("Aviso", args, "ok");
							
							paraPensar ();

							Navigation.PushModalAsync (new SideC ());
						});
						break;
					case("Falha"):
						Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
							DisplayAlert ("Aviso", args + " de rede.", "ok");
						});
						btL.IsEnabled = true;
						btS.IsEnabled = true;
						paraPensar ();
						break;

					}


				});
				MessagingCenter.Subscribe<Login, string> (this, "aviso", async (Login, args) => {
					Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
						DisplayAlert ("Aviso", args, "ok");
					});
				});

				if (usernameEntry.Text != "" && passwordEntry.Text != "") {
				
					Task task = new Task(async () => {
						await Cliente.getInstance (this).
						inicial (usernameEntry.Text, Hash(passwordEntry.Text));
					});
					task.Start();

			


				} else {
					Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
						DisplayAlert ("Aviso", "Preencha todos os campos", "ok");
						paraPensar ();

					});
				}
			} else {
				Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
					DisplayAlert ("Aviso", "Não está ligado à internet.", "ok");
					paraPensar ();

				});
			}

		}
			
		void OnSettingsClicked (object sender, EventArgs e)
		{
			Navigation.PushModalAsync (new Settings ());
			//Navigation.RemovePage (Login);

		}

		public string Hash(string input)
		{

			byte[] getSHA512 = System.Text.Encoding.UTF8.GetBytes (input);
			Org.BouncyCastle.	Crypto.Digests.Sha256Digest digester = new Org.BouncyCastle.Crypto.Digests.Sha256Digest();
			byte[] retValue = new byte[digester.GetDigestSize()];
			digester.BlockUpdate(getSHA512, 0, getSHA512.Length);
			digester.DoFinal(retValue, 0);
			var asd129 = BitConverter.ToString (retValue).Replace ("-", "");
			return asd129;
		}
	}
}

