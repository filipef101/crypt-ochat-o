﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;
//using Acr.UserDialogs;

namespace ChatoCrypto
{
	public partial class Settings : ContentPage
	{
		View cthis;
		public Settings ()
		{
			InitializeComponent ();
			Cliente.getInstance (this).retrieveset ();

			ipEntry.Text = Cliente.getInstance (this).Cip;
			portaEntry.Text = Cliente.getInstance (this).Cporta;
			salaEntry.Text = Cliente.getInstance (this).Csala;

			usoEntry.IsToggled= Refractored.Xam.Settings.CrossSettings.Current.GetValueOrDefault("uso", true);

			ApagaListaBt.Clicked +=async(sender,e)=>{ apagarLista(sender,e);};

			ChavesBt.Clicked +=async (sender, e) => {
				cthis =this.Content;
				this.Content=new StackLayout() {
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
					Children = {
						new  ActivityIndicator() {IsRunning=true,
							HorizontalOptions = LayoutOptions.CenterAndExpand , IsVisible=true
						},
						new Label(){Text="A gerar um novo par de chaves, vai demorar uns minutos aguarda..." }
					}
				};
				Task task = new Task(async () => {await NovaChaves();});
				task.Start();
				//UserDialogs.Init()
			};
		}
		void SaveSettings(object sender, EventArgs e){

			Refractored.Xam.Settings.CrossSettings.Current.AddOrUpdateValue ("uso", usoEntry.IsToggled);

			Cliente.getInstance(this).
			GuardaSettings(ipEntry.Text, portaEntry.Text, salaEntry.Text);

			Xamarin.Forms.Device.BeginInvokeOnMainThread(delegate {
				Navigation.PopModalAsync();
			});
		}
		async void apagarLista(object sender, EventArgs e){

			Cliente.getInstance (this).ApagaListaC ();
			Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
				Navigation.PopModalAsync ();
			});

		}
		async Task NovaChaves ()
		{
			await Cliente.getInstance (this).RecriaChaves ().ConfigureAwait(false);

			Xamarin.Forms.Device.BeginInvokeOnMainThread(async () =>{
				this.Content = cthis;
				Navigation.PopModalAsync ();
			});






		}

	}
}

