﻿using System;


using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace ChatoCrypto
{
	public partial class MyPage : ContentPage
	{
		ObservableCollection<string> Items;
		public MyPage ()
		{
			Items = new ObservableCollection<string> ();

			SideC  c = new SideC ();



			MessagingCenter.Subscribe<Login, string> ( this, "mensa", async (Login, args) => {
				// do something whenever the "Hi" message is sent
				Xamarin.Forms.Device.BeginInvokeOnMainThread(delegate {
					Items.Add(args);


				});

			});

			InitializeComponent ();
		//	liste.ItemsSource= 

			//usadors.ItemsSource = Cliente.listaU ();
			listView.ItemsSource = Items;
			MessagingCenter.Send<SideC, string> (new SideC(), "mostrar","Falha");
		}
		void OnEnviaClicked(object sender, EventArgs e){
			Cliente.enviaToUsr ( entUser.Text , entMsg.Text);
			Items.Add ("Eu: " + entMsg.Text);
		}

		public void OnItemSelected (object sender, SelectedItemChangedEventArgs e) {
			if (e.SelectedItem == null) return; // has been set to null, do not 'process' tapped event
			DisplayAlert("", e.SelectedItem.ToString(), "OK");
			((ListView)sender).SelectedItem = null; // de-select the row
		}
	}
}

