﻿using System;
using Xamarin.Forms;
using ChatSharp;
using ChatSharp.Events;
using ChatSharp.Handlers;
using Org.BouncyCastle;
using Refractored.Xam.Settings;
using System.Linq;
using System.Collections.ObjectModel;
using 	System.Runtime.Serialization.Json;
using System.Diagnostics;
using Org.BouncyCastle.Crypto;
using System.Text;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.OpenSsl;
using System.IO;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto.Generators;
using System.Collections.Generic;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Digests;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Pkcs;
using System.Threading.Tasks;


namespace ChatoCrypto
{
	public class Cliente
	{
		private static Cliente instance;
		private static Page page;

		private Cliente ()
		{
			retrieveset ();
		}

public static Cliente getInstance (Page thepage)
		{
			page = thepage;
			if (instance == null) {
				instance = new Cliente ();
			}  //
			return instance;
		}

		////////////////////////////////////////////////////////////////
		public string Cip;
		public string Cporta;
		public string Csala;
		static string salacae;
		static string Cuser;
		static string Cpwd;
		public bool Cssl;
		public IrcUser Usador;
		public IrcChannel canal;
		public Boolean ligado;
		public Boolean gerado;
		static IrcClient cliento;
		
		public string MinhaPubKey;
		public string myhash;

		public string[] msgArr;
		public string[] msgAr;
		public int logado;

		static	ObservableCollection<Contact> ListaC;

		static	AsymmetricCipherKeyPair Chavas;

		async public  Task inicial (string U, string P)
		{
			logado = 0;
			if (CrossSettings.Current.GetValueOrDefault <Boolean>("firstime",false)!=true) {
				Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
					MessagingCenter.Send<Login, string> (new Login (), "estado", "A gerar chaves, para sua segurança este processo pode demorar alguns minutos, aguarde.");


				});
				Chavas = novoParChaves ();
				ListaC = new ObservableCollection<Contact> ();

				/*
				JObject data = new JObject {
					{"private",JsonConvert.SerializeObject (Chavas.Private)},
					{"public",JsonConvert.SerializeObject(Chavas.Public)},
				};
				Debug.WriteLine (data.ToString ()); */
				PrivateKeyInfo privateKeyInfo = PrivateKeyInfoFactory.CreatePrivateKeyInfo(Chavas.Private);
				byte[] serializedPrivateBytes = privateKeyInfo.ToAsn1Object().GetDerEncoded();
				string serializedPrivate = Convert.ToBase64String(serializedPrivateBytes);


				CrossSettings.Current.AddOrUpdateValue<string> ("chavPriv",serializedPrivate);
				CrossSettings.Current.AddOrUpdateValue<string> ("chavPub",MPubKeySt());

				CrossSettings.Current.AddOrUpdateValue<Boolean> ("firstime", true);
			} else {
				
				ListaC = JsonConvert.DeserializeObject<ObservableCollection<Contact>> (
					Refractored.Xam.Settings.CrossSettings.Current.GetValueOrDefault<string> ("contactos",
						JsonConvert.SerializeObject (new ObservableCollection<Contact>() 
						)));


				string sPr = Refractored.Xam.Settings.CrossSettings.Current.GetValueOrDefault<string> ("chavPriv");
				string sPb = Refractored.Xam.Settings.CrossSettings.Current.GetValueOrDefault<string> ("chavPub");


				AsymmetricKeyParameter privateKey = readPrivateKey(sPr);

			
				AsymmetricKeyParameter p2 = ReadPublicKey(sPb);
											// public , privada
				Chavas = new AsymmetricCipherKeyPair (p2,privateKey);
			}
			myhash = Chavas.Public.GetHashCode ().ToString ()+ " "+CalculateMD5Hash(MPubKeySt());
			gerado = true;
			login (U, P);
		}

		private static string[] stringParaArrayX6 (string String)
		{
			int le=0;
			int a = 0;
			string[] st = new string[6];
			le = String.Length;
			while (le % 6 != 0) {
				le++;
				a++;
			}

				le = le / 6;


				st [0] = String.Substring (0,      le);  
				st [1] = String.Substring (le,     le);
				st [2] = String.Substring (le * 2, le);
				st [3] = String.Substring (le * 3, le);
				st [4] = String.Substring (le * 4, le );
				st [5] = String.Substring (le * 5,le-a); 



			return st;
		}

		private static string ArrayX6paraString (string[] array)
		{
			//if (array.GetLength () > 1)
			return array [0] + array [1] + array [2] + array [3] + array [4] + array [5];
			//else
			//	return "erro interno";
		}

		public string  MPubKeySt(){
			
			if (MinhaPubKey == null) {
				SubjectPublicKeyInfo publicKeyInfo = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo (Chavas.Public);
				byte[] serializedPublicBytes = publicKeyInfo.ToAsn1Object ().GetDerEncoded ();
				MinhaPubKey = Convert.ToBase64String (serializedPublicBytes);

				return MinhaPubKey;
			} else {
				return MinhaPubKey;
			}

		}
		public static void EnviaMyChaveUser (String ch, string user)
		{
			if (ch == null) {



				SubjectPublicKeyInfo publicKeyInfo = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(Chavas.Public);
				byte[] serializedPublicBytes = publicKeyInfo.ToAsn1Object().GetDerEncoded();
				string serializedPublic = Convert.ToBase64String(serializedPublicBytes);

				ch = serializedPublic;
			}
			ch = CompressString (ch);

			string[] st = stringParaArrayX6 (ch);

			cliento.SendMessage ("§K1&" + st [0], user);
			cliento.SendMessage ("§K2&" + st [1], user);
			cliento.SendMessage ("§K3&" + st [2], user);
			cliento.SendMessage ("§K4&" + st [3], user);
			cliento.SendMessage ("§K5&" + st [4], user);
			cliento.SendMessage ("§K6&" + st [5], user);

		}

		public static void EnviaMyChaveCanal (String ch)
		{
			if (ch == null) {
				TextWriter textWriter = new StringWriter ();
				PemWriter pemWriter = new PemWriter (textWriter);
				pemWriter.WriteObject (Chavas.Public);
				pemWriter.Writer.Flush ();

				ch = textWriter.ToString ();
			}
			ch = CompressString (ch);

			string[] st = stringParaArrayX6 (ch);

			cliento.NetworkError+= (object sender, SocketErrorEventArgs e) => {
				string asd = e.ToString();
			};
			
			cliento.SendMessage ("§K1&" + st [0], ch);
			cliento.SendMessage ("§K2&" + st [1], ch);
			cliento.SendMessage ("§K3&" + st [2], ch);
			cliento.SendMessage ("§K4&" + st [3], ch);
			cliento.SendMessage ("§K5&" + st [4], ch);
			cliento.SendMessage ("§K6&" + st [5], ch);
		}

		public static void enviaToUsr (string userE, string messagem)
		{
			if (messagem == "§P£") {
				cliento.SendMessage (messagem, userE);
			} else {
				string massage = null;

				massage = CompressString (messagem);
				massage = crypta (massage, userE);
				massage = CompressString (massage);

				string[] st = stringParaArrayX6 (massage);

				cliento.SendMessage ("§M1&" + st [0], userE);
				cliento.SendMessage ("§M2&" + st [1], userE);
				cliento.SendMessage ("§M3&" + st [2], userE);
				cliento.SendMessage ("§M4&" + st [3], userE);
				cliento.SendMessage ("§M5&" + st [4], userE);
				cliento.SendMessage ("§M6&" + st [5], userE);
			}
			cliento.RawMessageSent += (object sender, ChatSharp.Events.RawMessageEventArgs e) => {
				//cliento.SendMessage (messagem, userE);

			};
		}
		/// <summary>
		/// Compresses the string.
		/// </summary>
		/// <returns>The string.</returns>
		/// <param name="text">Text.</param>
		public static string CompressString (string text)
		{
			return Base64Encode (text);
		}

		/// <summary>
		/// Decompresses the string.
		/// </summary>
		/// <returns>The string.</returns>
		/// <param name="compressedText">Compressed text.</param>
		public static string DecompressString (string compressedText)
		{
			return Base64Decode (compressedText);	
		}
		public static string Base64Encode(string plainText) {
			var bytes = Encoding.UTF8.GetBytes(plainText);
			string base64 = Convert.ToBase64String(bytes);
			return base64;
		}



		public static string Base64Decode(string base64EncodedData) {
			
			var data= Convert.FromBase64String (base64EncodedData);	
			return Encoding.UTF8.GetString (data,0,data.Length);
		}
		public  void login (String user, String pwd)
		{
			salacae = Csala;

			Cuser = user;
			Usador = new IrcUser (Cuser, Cuser);
			cliento = new IrcClient (Cip, Usador, Cssl);

			cliento.ChannelListRecieved += (object sender, ChatSharp.Events.ChannelEventArgs e) => {
				ligado = true;
				var asd = e.Channel.Name;
				if ((cliento.Channels.Count () > 0) && (ligado == true && gerado==true )) {
					
					if (logado==0) {
						Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
							MessagingCenter.Send<Login, string> (new Login (), "log", "Logado"); });
					}
						else {
						var var297=logado;
					}
					logado++;
				} 
			};


			
			cliento.ConnectionComplete += (object sender, EventArgs e) => {
				

				//this.DisplayAlert ("Aviso","ligação completa a entrar na sala.  " + e.ToString (),"OK");
				//cliento.JoinChannel (Csala);
			//	cliento.Channels.Join (Csala);
				cliento.JoinChannel ("#" + Csala);
			//	cliento.Channels.Join ("#" + Csala);

				Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
					
					
				});



			};
			cliento.PrivateMessageRecieved += (object sender, ChatSharp.Events.PrivateMessageEventArgs e) => {
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////7
				/// /// /// /// /// /// /// /// /// ////// ////// /// /// /// /// // // // ///// //    ///  ///  // / / / / ///
				string msg = null;
				// :FILIAAs!~  apanhar essa merda e 

//				passar para de
				if (e.PrivateMessage.IsChannelMessage) {
					Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
						MessagingCenter.Send<Login, string> (new Login (), "mensa","# "+ e.PrivateMessage.Source +" , "+ e.PrivateMessage.User + ": " + e.PrivateMessage.Message.ToString ());
					});

				}else if (e.PrivateMessage.Message.ToString ().StartsWith ("§P£")) {	
					EnviaMyChaveUser(MinhaPubKey,e.PrivateMessage.Source);
					Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
					MessagingCenter.Send<Login, string> (new Login (), "mensa","Chave Publica Enviada a "+e.PrivateMessage.Source );
					MessagingCenter.Send<Login, string> (new Login (), "mensa","Verifica se a Chave enviada corresponde á recebida:");
					MessagingCenter.Send<Login, string> (new Login (), "mensa",myhash);
					});


				}

				else if (e.PrivateMessage.Message.ToString ().StartsWith ("§M")) {


					if (e.PrivateMessage.Message.ToString ().StartsWith ("§M1")) {
						msgAr = new string[6];

						msgAr [0] = e.PrivateMessage.Message.ToString ().Substring (4);
					} else if (e.PrivateMessage.Message.ToString ().StartsWith ("§M2")) {

						msgAr [1] = e.PrivateMessage.Message.ToString ().Substring (4);
					} else if (e.PrivateMessage.Message.ToString ().StartsWith ("§M3")) {

						msgAr [2] = e.PrivateMessage.Message.ToString ().Substring (4);
					} else if (e.PrivateMessage.Message.ToString ().StartsWith ("§M4")) {

						msgAr [3] = e.PrivateMessage.Message.ToString ().Substring (4);
					} else if (e.PrivateMessage.Message.ToString ().StartsWith ("§M5")) {

						msgAr [4] = e.PrivateMessage.Message.ToString ().Substring (4);
					} else if (e.PrivateMessage.Message.ToString ().StartsWith ("§M6")) {

						msgAr [5] = e.PrivateMessage.Message.ToString ().Substring (4);
						string de=ArrayX6paraString (msgAr);
					

						msg=deCryptame (DecompressString (de));
					msg = DecompressString (msg);
						Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
						MessagingCenter.Send<Login, string> (new Login (), "mensa", e.PrivateMessage.Source + ": " + msg);
						});
					}


				} else if (e.PrivateMessage.Message.ToString ().StartsWith ("§K")) {
					if (e.PrivateMessage.Message.ToString ().StartsWith ("§K1")) {
						msgArr = new string[6];
						msgArr [0] = e.PrivateMessage.Message.ToString ().Substring (4);

					} else if (e.PrivateMessage.Message.ToString ().StartsWith ("§K2")) {
						
						msgArr [1] = e.PrivateMessage.Message.ToString ().Substring (4);
					} else if (e.PrivateMessage.Message.ToString ().StartsWith ("§K3")) {
						
						msgArr [2] = e.PrivateMessage.Message.ToString ().Substring (4);
					} else if (e.PrivateMessage.Message.ToString ().StartsWith ("§K4")) {
						
						msgArr [3] = e.PrivateMessage.Message.ToString ().Substring (4);
					} else if (e.PrivateMessage.Message.ToString ().StartsWith ("§K5")) {
						
						msgArr [4] = e.PrivateMessage.Message.ToString ().Substring (4);
					} else if (e.PrivateMessage.Message.ToString ().StartsWith ("§K6")) {
						
						msgArr [5] = e.PrivateMessage.Message.ToString ().Substring (4);
						string de=ArrayX6paraString (msgArr);
						if (estaNaLista( e.PrivateMessage.Source)){
							MessagingCenter.Send<Login, string> (new Login (), "mensa", "O utilzador "+e.PrivateMessage.Source+" tentou enviar uma chave publica, mas ele já está nos teus contactos.");

						}
						else{
						msg = DecompressString (de);
						Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
						MessagingCenter.Send<Login, string> (new Login (), "mensa", "O utilzador "+e.PrivateMessage.Source+" enviou a sua Chave");
						MessagingCenter.Send<Login, string> (new Login (), "mensa", "Verifique se a Chave é igual:");
						MessagingCenter.Send<Login, string> (new Login (), "mensa", ReadPublicKey(msg).GetHashCode().ToString()+ " "+CalculateMD5Hash(msg));
						});
						GuardaChave (e.PrivateMessage.Source, msg);
						}
					}



				
				}


			};/////////////////////////////////////////////////////////////////////////////////////////
			cliento.ServerInfoRecieved += (object sender, SupportsEventArgs e) => {
				var asd=e;
			};
			cliento.UserPartedChannel +=  (sender, e) => {
				Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
					MessagingCenter.Send<Login, string> (new Login (), "de", e.User.Nick);
				}
				);
			};

				
			cliento.RawMessageRecieved += (object sender, ChatSharp.Events.RawMessageEventArgs e) => {
				Debug.WriteLine(e.Message);
				//bool estado=true;
				if (e.Message.ToLower().Contains("quit"))  Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
					MessagingCenter.Send<SideC, string> (new SideC (), "de", e.Message.Substring(e.Message.IndexOf(":"),e.Message.IndexOf("!")-e.Message.IndexOf(":")));
				}
				);
				bool estado= ( (e.Message.ToLower().Contains("error") || e.Message.ToLower().Contains("banned") ||(e.Message.ToLower().Contains("quit") || e.Message.ToLower().Contains("failed") )));
				if (estado){
					if (ligado == true && gerado==true) {Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
						
						MessagingCenter.Send<Login, string> (new Login (), "mensa", "Servidor: " + e.Message.ToString ()); });
					}
					else if (e.Message.ToLower().Contains("quit") || e.Message.ToLower().Contains("failed") || e.Message.ToLower().Contains("error") ){Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
						MessagingCenter.Send<Login, string> (new Login (), "log", "Falha");});
					}
				}else{Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
					MessagingCenter.Send<Login, string> (new Login (), "estado",  e.Message.ToString ());});
				}
			
			};

			cliento.NetworkError += (object sender, ChatSharp.Events.SocketErrorEventArgs e) => {
				ligado = false;
				Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
					MessagingCenter.Send<Login, string> (new Login (), "log", "Falha");});
			};


			cliento.NickInUse += (object sender, ChatSharp.Events.ErronousNickEventArgs e) => {
				Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
				MessagingCenter.Send<Login, string> (new Login (), "aviso", "nome de utilizadar em uso, mudado para " + e.NewNick);
				});
			//	cliento.JoinChannel (Csala);
			//	cliento.Channels.Join (Csala);
			};

			cliento.UserJoinedChannel+= async delegate(object sender, ChannelUserEventArgs e) {
				Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {
					MessagingCenter.Send<Login, string> (new Login (), "du", e.User.Nick);});
			};
					

			Cpwd = pwd;
			Cuser = user;


			//cliento.Quit ("novo login"); //sair e indicar a razao de saída

			cliento.User = Usador;
			
			cliento.Settings.GenerateRandomNickIfRefused = Refractored.Xam.Settings.CrossSettings.Current.GetValueOrDefault("uso", true);
			try{
			cliento.ConnectAsync ();
			}
			catch{Xamarin.Forms.Device.BeginInvokeOnMainThread (delegate {

				MessagingCenter.Send<Login, string> (new Login (), "log", "Falha");

			});
			}
			}

		public static Boolean ligarSv (String ip, String porta)
		{
			
			return false;
		}

		public  void GuardaSettings (string ip, string porta, string sala)
		{

			Cip = ip;
			Cporta = porta;
			Csala = sala;

			this.saveset ();
		}

		public ObservableCollection<string>  listaU ()
		{
			ObservableCollection<string> lista;



			lista = new ObservableCollection<string> ();
			//string a = Csa
	
			if (cliento.Channels.Count () > 0) {
				foreach (IrcChannel c in cliento.Channels) {
					foreach (IrcUser u in c.Users) {
						lista.Add (u.Nick);
					}
				}

			} else {
				lista.Add ("Sem utilizadores na sala");
			}
			return lista;
		}

		public void saveset ()
		{	//store
			Refractored.Xam.Settings.CrossSettings.Current.AddOrUpdateValue ("username", Cuser);
			Refractored.Xam.Settings.CrossSettings.Current.AddOrUpdateValue ("porta", Cporta);
			Refractored.Xam.Settings.CrossSettings.Current.AddOrUpdateValue ("sala", Csala);
			Refractored.Xam.Settings.CrossSettings.Current.AddOrUpdateValue ("ip", Cip);
			Refractored.Xam.Settings.CrossSettings.Current.AddOrUpdateValue ("ssl", Cssl);
		}
				// Function called from OnCreate
		public void retrieveset ()
		{	//retreive 
			



			Cuser = Refractored.Xam.Settings.CrossSettings.Current.GetValueOrDefault ("username", "");
			Cporta = Refractored.Xam.Settings.CrossSettings.Current.GetValueOrDefault ("porta", "6667");
			Csala = Refractored.Xam.Settings.CrossSettings.Current.GetValueOrDefault ("sala", "sala");
			Cip = Refractored.Xam.Settings.CrossSettings.Current.GetValueOrDefault ("ip", "irc.shaw.ca");
			Cssl = Refractored.Xam.Settings.CrossSettings.Current.GetValueOrDefault ("ssl", false);


		}

		public void GuardaChave (string user, string chavP)

		{
			if (ListaC == null)
				ListaC = new ObservableCollection<Contact> ();
			if (estaNaLista(user)==false){
			Contact cont = new Contact ();
			cont.Chave = chavP;
			cont.Nome = user;
			ListaC.Add (cont);
			GuardaCCJ ();
			}

		}
		public async void GuardaCCJ(){
			string json = JsonConvert.SerializeObject (ListaC);
			CrossSettings.Current.AddOrUpdateValue<string>("contactos",json);



		}

		public bool estaNaLista(string noma){
			Contact conta = null;

			conta = ListaC.FirstOrDefault (i => i.Nome == noma);

			if (conta != null) { return true;
			} else { 			 return false;
			}
		}

		static  string crypta (string msg, string User)
		{	string encrptada = " ";
			Contact conta = null;

			conta = ListaC.FirstOrDefault (i => i.Nome == User);

			if (conta != null) {
				encrptada = RSAEncrypt (msg, ReadPublicKey (conta.Chave));
			} else {
				
			}
			return encrptada;
		}

		public string deCryptame (string msg){
			return RSADecrypt (msg, Chavas.Private );		
		}

		public AsymmetricCipherKeyPair novoParChaves ()
		{	const int RsaKeySize = 2148;
			
			CryptoApiRandomGenerator randomGenerator = new CryptoApiRandomGenerator ();
			SecureRandom secureRandom = new SecureRandom (randomGenerator);

			var keyGenerationParameters = new KeyGenerationParameters (secureRandom, RsaKeySize);
			var keyPairGenerator = new RsaKeyPairGenerator ();
			keyPairGenerator.Init (keyGenerationParameters);
			return keyPairGenerator.GenerateKeyPair ();
		}

		public static  AsymmetricKeyParameter readPrivateKey (string privateKey)
		{	
			var asd1 = Convert.FromBase64String (privateKey);
			AsymmetricKeyParameter pKey = (RsaPrivateCrtKeyParameters) PrivateKeyFactory.CreateKey(asd1);

			return pKey;
		}
		public static  AsymmetricKeyParameter ReadPublicKey(string PublicKey)
		{	
			
			AsymmetricKeyParameter pulicKey = (AsymmetricKeyParameter) PublicKeyFactory.CreateKey(Convert.FromBase64String(PublicKey));
			return pulicKey;
		}

		public static string RSADecrypt(string Sdata, AsymmetricKeyParameter key)
		{	var bytesToDecrypt = Convert.FromBase64String(Sdata);
			var decryptEngine = new Pkcs1Encoding(new RsaEngine());				
				decryptEngine.Init(false, key);
			var as123 = decryptEngine.ProcessBlock (bytesToDecrypt, 0, bytesToDecrypt.Length);
			var decrypted = Encoding.UTF8.GetString(as123,0,as123.Length);
			return decrypted;
		}
			
		public static string RSAEncrypt(string Sdata, AsymmetricKeyParameter key)
		{		var bytesToEncrypt = Encoding.UTF8.GetBytes(Sdata);
				var encryptEngine = new Pkcs1Encoding(new RsaEngine());
				encryptEngine.Init(true, key);
				var encrypted = Convert.ToBase64String(encryptEngine.ProcessBlock(bytesToEncrypt, 0, bytesToEncrypt.Length));
				return encrypted;
		}


		public string CalculateMD5Hash(string input)
		{
			
			byte[] getSHA512 = Encoding.UTF8.GetBytes (input);
			Org.BouncyCastle.	Crypto.Digests.Sha512Digest digester = new Org.BouncyCastle.Crypto.Digests.Sha512Digest();
				byte[] retValue = new byte[digester.GetDigestSize()];
			digester.BlockUpdate(getSHA512, 0, getSHA512.Length);
				digester.DoFinal(retValue, 0);

			return BitConverter.ToString (retValue);
		}

		public void ApagaListaC(){
			ListaC = new ObservableCollection<Contact> ();
		}
		public async Task RecriaChaves(){

			MinhaPubKey = null;
			Chavas = null;
			Chavas=novoParChaves ();


			/*
				JObject data = new JObject {
					{"private",JsonConvert.SerializeObject (Chavas.Private)},
					{"public",JsonConvert.SerializeObject(Chavas.Public)},
				};
				Debug.WriteLine (data.ToString ()); */
			PrivateKeyInfo privateKeyInfo = PrivateKeyInfoFactory.CreatePrivateKeyInfo(Chavas.Private);
			byte[] serializedPrivateBytes = privateKeyInfo.ToAsn1Object().GetDerEncoded();
			string serializedPrivate = Convert.ToBase64String(serializedPrivateBytes);


			CrossSettings.Current.AddOrUpdateValue<string> ("chavPriv",serializedPrivate);
			CrossSettings.Current.AddOrUpdateValue<string> ("chavPub",MPubKeySt());
		}
	}
}

